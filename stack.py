class ProdCaster:
    def __init__(self):
        self.messages = []
    def is_NoMessage(self):
        return self.messages == []
    def add_message(self,message):
        self.messages.append(message)
    def pop_message(self):
        return self.messages.pop()
    def size_Messages(self):
        return len(self.messages)