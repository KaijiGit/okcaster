# REQUIREMENTS
- Python 3.7
- Discord Py
# INSTALL
- pip install discord
# USAGE
- b=help : Show help message.
- b=message : Add message.
- b=clean : Clear the message.
- b=send?everyone : Send the message to everyone.
- b=send?role @Rolename : Send the message to a specific group.