import discord
# data structure (Stack Structure)
from stack import ProdCaster

# Give access for using the bot
global owner_ids
owners_ids = [""] # Add the owners ids to give them access to the bot (ex : Apollo#9468)
# Token of the bot
TOKEN = ""

client = discord.Client()
controller = ProdCaster()


@client.event
async def on_message(message):
    if message.content.startswith("b=message"):
        if str(message.author) in owners_ids:
            array_message = message.content.split(" ", 1)
            if len(array_message) > 1 and controller.is_NoMessage():
                prod_message = array_message[1]
                controller.add_message(prod_message)
                await message.channel.send("ثم اضافة رسالتك")
            else:
                await message.channel.send("يبدو انك لم تكتب الرسالة او ان هنالك رسالة اخرى")
        else:
            await message.channel.send("ليس لديك الصلاحيات")

    if message.content.startswith("b=show"):
        if str(message.author) in owners_ids:
            if controller.is_NoMessage() == False:
                message_to_send = controller.pop_message()
                controller.add_message(message_to_send)
                await message.channel.send(message_to_send)
            else:
                await message.channel.send("لا يوجد رسالة من اجل اظهارها")
        else:
            await message.channel.send("ليس لديك الصلاحيات من اجل رأية الرسائل")

    if message.content == "b=clean":
        if str(message.author) in owners_ids:
            if controller.is_NoMessage() == False:
                controller.pop_message()
                await message.channel.send('ثم مسح سجل الرسائل')
            else:
                await message.channel.send("لا توجد هنالك رسالة من اجل مسحها من السجل")
        else:
            await message.channel.send('ليس لديك صلاحيات لاستعمال هاذا الامر')

    if message.content == "b=send?everyone":
        if str(message.author) in owners_ids:
            if controller.is_NoMessage() == False:
                try:
                    members = client.get_all_members()
                    message_to_send = controller.pop_message()
                    for member in members:
                        await member.send(message_to_send)
                except Exception:
                    pass
                finally:
                    await message.channel.send("ثم ارسال جميع الرسائل")
            else:
                await message.channel.send("لا توجد رسالة من اجل ارسالها")
        else:
            await message.channel.send('ليس لديك صلاحيات لاستعمال هاذا الامر')

    if message.content.startswith("b=send?role"):
        if str(message.author) in owners_ids:
            if controller.is_NoMessage() == False:
                try:
                    members = client.get_all_members()
                    searched_role = message.content.split(" ",1)[1]
                    searched_role = searched_role.translate({ord(i): None for i in '<@&>'})
                    message_to_send = controller.pop_message()
                    for member in members:
                        for role in member.roles:
                            if int(role.id) == int(searched_role):
                                await member.send(message_to_send)
                except Exception:
                    pass
                finally:
                    await message.channel.send("ثم ارسال جميع الرسائل")
            else:
                await message.channel.send("لا توجد رسالة من اجل ارسالها")
        else:
            await message.channel.send('ليس لديك صلاحيات لاستعمال هاذا الامر')
    
    if message.content == "b=help":
        await message.channel.send("""```افضل بوت برودكاست في التاريخ طبعا
b=help      -  من اجل اظهار هذه الرسالة
b=message   -  من اجل اضافة الرسالة
b=clean     -  من اجل مسح الرسالة
b=send?everyone   -  ارسال الرسالة للجميع
b=send?role @Rolename  -  ارسال الرسالة لمجموعة من الاشخاص يمتلكون رول معين```""")

client.run(TOKEN)
